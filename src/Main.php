<?php

namespace App;

use App\Service\SortService;
class Main
{
    private SortService $sortService;

    public function __construct()
    {
        $this->sortService = new SortService();
    }

    public function sortBoardingCards()
    {
        $this->sortService->sortBoardingCards(

        );
    }
}