<?php

namespace App\DTO;

class BoardingCardPath
{
    private string $from;
    private string $to;
    private array $boardingCards;

    public function __construct(
        string $from,
        string $to
    ) {
        $this->from = $from;
        $this->to = $to;
    }


    public function addBoardingCard(array $boardingCard): void
    {
        $this->boardingCards[$boardingCard['from']] = $boardingCard;
    }

    public function getBoardingCards(): array
    {
        return $this->boardingCards;
    }

    public function getFrom(): string
    {
        return $this->from;
    }

    public function setFrom(string $from): void
    {
        $this->from = $from;
    }

    public function getTo(): string
    {
        return $this->to;
    }

    public function setTo(string $to): void
    {
        $this->to = $to;
    }

}