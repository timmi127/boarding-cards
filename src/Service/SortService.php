<?php

namespace App\Service;

use App\DTO\BoardingCardPath;

class SortService
{
    private const BOARDING_CARDS = [
        [
            "from" => "Moscow",
            "to" => "New York",
            "type" => "flight",
            "description" => "Gate 45B, seat 3A. Baggage drop at ticket counter 344."
        ],
        [
            "from" => "Barcelona",
            "to" => "Madrid",
            "type" => "train 78A",
            "description" => "Sit in seat 45B"
        ],
        [
            "from" => "Dubai",
            "to" => "Tashkent",
            "type" => "boat",
            "description" => "Gate 45B, seat 3A."
        ],
        [
            "from" => "Tashkent",
            "to" => "Tbilisi",
            "type" => "boat",
            "description" => "Gate 45B, seat 3A."
        ],
        [
            "from" => "Madrid",
            "to" => "Moscow",
            "type" => "flight",
            "description" => "Gate 45B, seat 3A. Baggage drop at ticket counter 344."
        ],
        [
            "from" => "New York",
            "to" => "Kazan",
            "type" => "train",
            "description" => "Gate 45B, seat 3A. Baggage drop at ticket counter 344."
        ],
        [
            "from" => "Tbilisi",
            "to" => "Pargue",
            "type" => "boat",
            "description" => "Gate 45B, seat 3A."
        ],
        [
            "from" => "Bali",
            "to" => "Barcelona",
            "type" => "flight",
            "description" => "Sit in seat 45B"
        ],
        [
            "from" => "Kazan",
            "to" => "Dubai",
            "type" => "car",
            "description" => "Gate 45B, seat 3A."
        ]
    ];


    public function sortBoardingCards(): void
    {
        $boardingCards = self::BOARDING_CARDS;

        $boardingCardPath = new BoardingCardPath(
            $boardingCards[0]['from'],
            $boardingCards[0]['to'],
        );

        $boardingCardPath->addBoardingCard($boardingCards[0]);
        unset($boardingCards[0]);
        $this->sort($boardingCardPath, $boardingCards);

        $sortedBoardingCards = [];

        $boardingCards = $boardingCardPath->getBoardingCards();
        $boardingCardsCount = count($boardingCards);
        $currentPoint = $boardingCardPath->getFrom();
        // We need 1 more loop, because array_unshift function complexity is O(n).
        // And it's so expensive to use for any addition to head a sorted array
        for ($i = 0; $i < $boardingCardsCount; $i ++) {
            $sortedBoardingCards[] = $boardingCards[$currentPoint];
            $currentPoint = $boardingCards[$currentPoint]['to'];
        }

        echo json_encode($sortedBoardingCards);die;
    }

    private function sort(BoardingCardPath $boardingCardPath, array $boardingCards): void
    {
        //best algorithm complexity O(n)
        //worst algorithm complexity ~ O(n * n / 2)
        $newBoardingCards = [];

        foreach ($boardingCards as $boardingCard) {
            if ($boardingCardPath->getFrom() === $boardingCard['to']) {
                $boardingCardPath->addBoardingCard($boardingCard);
                $boardingCardPath->setFrom($boardingCard['from']);
            } elseif ($boardingCardPath->getTo() === $boardingCard['from']) {
                $boardingCardPath->addBoardingCard($boardingCard);
                $boardingCardPath->setTo($boardingCard['to']);
            } else {
                $newBoardingCards[] = $boardingCard;
            }
        }
        if (count($newBoardingCards) === 0) {
            return;
        }

        $this->sort($boardingCardPath, $newBoardingCards);
    }
}